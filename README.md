# UnAPI: Unicorh Hybrid Black Python API #

Based on ctypes binding with [Unicorn C API library provided by g.tec](https://github.com/unicorn-bi/Unicorn-Suite-Hybrid-Black).

## Installation ##

To install the module as dependency in your Python project simply type:
```
pip install git+https://barleyjuice@bitbucket.org/barleyjuice/unicorn-api.git#egg=unicorn-api
```
If the installation was successful, you will be able to import the module in your project:
```python
import unapi
```

## Use ##

To begin working with the API import the Unicorn class from Unicorn module. Then create new Unicorn instance using the empty constructor:

```python
from unapi import Unicorn

bci = Unicorn()
```
Now you can interact with your device using the `bci` instance. There API includes all the functions related to official Unicorn C API. For examples see [examples.py](./examples.py).