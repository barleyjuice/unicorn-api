from distutils.extension import Extension
from setuptools import setup

setup(
    name='unicorn-api',
    version='0.2.4',
    author='Aliona Petrova',
    author_email='consciencee95@gmail.com',
    packages=['unapi'],
    url='https://bitbucket.org/barleyjuice/unicorn-api/',
    description='Unicorn Hybrid Black Python API based on C library binding',
    long_description=open('README.md').read(),
    include_package_data=True
)
