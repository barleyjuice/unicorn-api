from unapi import Unicorn
import traceback

bci = Unicorn()

print("========== Getting the API version ==========")
print("=============================================\n")

print(f"API version {bci.getApiVersion()}\n")

print("======== Getting the last error text ========")
print("=============================================\n")

print(f"Error: {bci.getLastErrorText()}\n")

print("=========== Getting Bluetooth info =========")
print("=============================================\n")

try:
    ble = bci.getBluetoothAdapterInfo()
    print(f"Bluetooth info:\n name: {ble.name}, manufacturer: {ble.manufacturer}, \
    recommended: {'yes' if ble.isRecommendedDevice else 'no'}, problems: {'yes' if ble.hasProblem else 'no'}")
except Exception as exc:
    print(exc)

print()
print("======== Getting available devices ==========")
print("=============================================\n")

handle = 0

try:
    (availableDevices, availableDevicesCount) = bci.getAvailableDevices()
    print(f"Available devices: {availableDevices}, count: {availableDevicesCount}")
    handle = availableDevices[0]
except Exception as exc:
    print(exc)
    
print()
print("============ Opening the device =============")
print("=============================================\n")

try:
    handle = bci.openDevice(handle)
    print(f"Device open. Handle: {handle}")
except:
    traceback.print_exc()
    
print()
print("========== Start data acquisition ===========")
print("=============================================\n")

try:
    bci.startAcquisition(handle)
    print(f"Acquisition started. Handle: {handle}")
except:
    traceback.print_exc()

print()
print("=========== Get acquired data ===============")
print("=============================================\n")

try:
    data = bci.getData(handle, n_samples=1)
    print(f"Acquired data: {len(data)}")
    for (i, sample) in enumerate(data):
        print(f"index: {i}, data: {sample}")
except:
    traceback.print_exc()
    
print()
print("========== Stop data acquisition ============")
print("=============================================\n")

try:
    bci.stopAcquisition(handle)
    print(f"Acquisition stopped. Handle: {handle}")
except:
    traceback.print_exc()
    
print()
print("======== Get channel configuration ==========")
print("=============================================\n")

try:
    configuration = bci.getConfiguration(handle)
    print(f"Configuration:")
    for channel in configuration.channels:
        print(f"Channel {channel.to_string()}")
    
    print("======== Set channel configuration ==========")
    print("=============================================\n")

    configuration.channels[0].enabled = True
    bci.setConfiguration(handle, configuration)
    print(f"Set configuration:")
    for channel in configuration.channels:
        print(f"Channel {channel.to_string()}")
except:
    traceback.print_exc()
    
    
print()
print("========== Get channel index ================")
print("=============================================\n")

channel_name = b"EEG 1"
try:
    index = bci.getChannelIndex(handle, channel_name)
    print(f"Index for channel {channel_name}: {index}")
except:
    traceback.print_exc()
    
print()
print("=========== Get number of channels ==========")
print("=============================================\n")

try:
    n_channels = bci.getNumberOfAcquiredChannels(handle)
    print(f"Number of acquired channels: {n_channels}")
except:
    traceback.print_exc()
    
print()
print("========== Get device information ===========")
print("=============================================\n")

try:
    device_info = bci.getDeviceInformation(handle)
    print(f"Device information: {device_info.to_string()}")
except:
    traceback.print_exc()
    
print()
print("========== Set digital outputs ==============")
print("=============================================\n")

try:
    bci.setDigitalOutputs(handle, 0b00000000)
    print(f"Setting digital levels.")
except:
    traceback.print_exc()
    
print()
print("========== Get digital outputs ==============")
print("=============================================\n")

try:
    levels = bci.getDigitalOutputs(handle)
    print(f"Got digital levels: {levels}")
except:
    traceback.print_exc()

print()
print("============ Closing the device =============")
print("=============================================\n")

try:
    bci.closeDevice(handle)
    print(f"Device close. Handle: {handle}")
except:
    traceback.print_exc()
    