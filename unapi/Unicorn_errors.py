
class UnicornInvalidParameterError(RuntimeError):
    def __str__(self):
        return "One of the specified parameters does not contain a valid value."

class UnicornBluetoothInitFailedError(RuntimeError):
    def __str__(self):
        return "The initialization of the Bluetooth adapter failed."

class UnicornBluetoothSocketFailedError(RuntimeError):
    def __str__(self):
        return "The operation could not be performed because the Bluetooth socket failed."

class UnicornOpenDeviceFailedError(RuntimeError):
    def __str__(self):
        return "The device could not be opened."

class UnicornInvalidConfigurationError(RuntimeError):
    def __str__(self):
        return "The configuration is invalid."

class UnicornBufferOverflowError(RuntimeError):
    def __str__(self):
        return "The acquisition buffer is full."

class UnicornBufferUnderflowError(RuntimeError):
    def __str__(self):
        return "The acquisition buffer is empty."

class UnicornOperationNotAllowedError(RuntimeError):
    def __str__(self):
        return "The operation is not allowed during acquisition or non-acquisition."

class UnicornConnectionProblemError(RuntimeError):
    def __str__(self):
        return "The operation could not complete because of connection problems."

class UnicornUnsupportedDeviceError(RuntimeError):
    def __str__(self):
        return "The device is not supported with this API."

class UnicornInvalidHandleError(RuntimeError):
    def __str__(self):
        return "The specified connection handle is invalid."

class UnicornGeneralError(RuntimeError):
    def __str__(self):
        return "An unspecified error occurred."

class UnicornUnknownError(RuntimeError):
    def __init__(self, code):
        self.code = code

    def __str__(self):
        return f"Unknown error code {self.code} was retrieved; Unicorn error is not recognized."

error_dict = {
        1: UnicornInvalidParameterError,
        2: UnicornBluetoothInitFailedError,
        3: UnicornBluetoothSocketFailedError,
        4: UnicornOpenDeviceFailedError,
        5: UnicornInvalidConfigurationError,
        6: UnicornBufferOverflowError,
        7: UnicornBufferUnderflowError,
        8: UnicornOperationNotAllowedError,
        9: UnicornConnectionProblemError,
        10: UnicornUnsupportedDeviceError,
        -2: UnicornInvalidHandleError,
        -1: UnicornGeneralError
}

class UnicornError:
    def __new__(cls, *args):
        code = args[0]
        if code in error_dict:
            return error_dict[code]
        else:
            return UnicornUnknownError(code)

    # def __init__(self, code) -> None:
    #     if(code != 0):
    #         if code in error_dict:
    #             raise error_dict[code]
    #         else:
    #             raise UnicornUnknownError(code)