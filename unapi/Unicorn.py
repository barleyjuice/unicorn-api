from ctypes import *
from .Unicorn_errors import *
import sys
import os

dir = os.path.dirname(sys.modules['unapi'].__file__)

shared_lib_path = os.path.join(dir, "lib/libunicorn.so")
if sys.platform.startswith('win32'):
    shared_lib_path = os.path.join(dir, "lib/Unicorn.dll")

lib = cdll.LoadLibrary(shared_lib_path)

UNICORN_STRING_LENGTH_MAX = 255
UNICORN_SERIAL_LENGTH_MAX = 14
UNICORN_TOTAL_CHANNELS_COUNT = 17
UNICORN_FIRMWARE_VERSION_LENGTH_MAX = 12

class UnicornBluetoothAdapterInfo(Structure):
    """
    The type that holds information about the Bluetooth adapter.
    """
    _fields_ = [
        # The name of the Bluetooth adapter used.
        ("name", c_char * UNICORN_STRING_LENGTH_MAX),

        # The manufacturer of the Bluetooth adapter.
        ("manufacturer", c_char * UNICORN_STRING_LENGTH_MAX),

        # Indicating if the used Bluetooth adapter is a recommended (delivered) device.
        ("isRecommendedDevice", c_bool),

        # Indicates whether the Bluetooth adapter has reported a problem or not.
        ("hasProblem", c_bool)
    ]

# Type that holds device serial.
UnicornDeviceSerial = c_char * UNICORN_SERIAL_LENGTH_MAX

# Type that holds the handle associated with a device.
UnicornHandle = c_uint64

# Type that holds firmware version.
UnicornFirmwareVersion = c_char * UNICORN_FIRMWARE_VERSION_LENGTH_MAX

# Type that holds device version.
UnicornDeviceVersion = c_char * 6

class UnicornDeviceInformation_C(Structure):
    """
    Type that holds additional information about the device.
    """
    _fields_ = [
        # The number of EEG channels.
        ("numberOfEegChannels", c_uint16),

        # The serial number of the device.
        ("serial", UnicornDeviceSerial),

        # The firmware version number.
        ("firmwareVersion", UnicornFirmwareVersion),

        # The device version number.
        ("deviceVersion", UnicornDeviceVersion),

        # The PCB version number.
        ("pcbVersion", c_uint8 * 4),

        # The enclosure version number.
        ("enclosureVersion", c_uint8 * 4)
    ]

class UnicornDeviceInformation:
    def __init__(self, device_info_c):
        self.n_eeg_channels = device_info_c.numberOfEegChannels
        self.serial = str(device_info_c.serial)
        self.firmvare_version = str(device_info_c.firmwareVersion)
        self.device_version = str(device_info_c.deviceVersion)
        self.pcb_version = device_info_c.pcbVersion
        self.enclosure_version = device_info_c.enclosureVersion
    
    def to_string(self):
        return f"number of eeg channels: {self.n_eeg_channels}; serial: {self.serial}; \
            firmware version: {self.firmvare_version}; device version: {self.device_version}; \
            PCB version: {[*self.pcb_version]}; enclosure version: {[*self.enclosure_version]}"

    def to_dict(self):
        return {
            "n_eeg_channels": self.n_eeg_channels,
            "serial": self.serial,
            "firmvare_version": self.firmvare_version,
            "device_version": self.device_version,
            "pcb_version": self.pcb_version,
            "enclosure_version": self.enclosure_version
        }

    def to_device_info_c(self):
        return UnicornDeviceInformation_C(self.n_eeg_channels, bytes(self.serial), bytes(self.firmvare_version), bytes(self.device_version), self.pcb_version, self.enclosure_version)

class UnicornAmplifierChannel_C(Structure):
    """
    The type containing information about a single channel of the amplifier.
    """
    _fields_ = [
        # The channel name.
        ("name", c_char * 32), 

        # The channel unit.
        ("unit", c_char * 32), 

        # The channel range as float array. First entry min value; Second max value.
        ("range", c_float * 2), 

        # The channel enabled flag. TRUE to enable channel; FALSE to disable channel.
        ("enabled", c_bool)
    ]

class UnicornAmplifierChannel:
    def __init__(self, channel_c):
        self.name = channel_c.name
        self.unit = channel_c.unit
        self.range = (channel_c.range[0], channel_c.range[1])
        self.enabled = channel_c.enabled
        self.index = None

    def set_index(self, index):
        self.index = index
    
    def to_dict(self):
        return {
            "index": self.index, "name": self.name, "unit": self.unit, "range": self.range, "enabled": self.enabled
        }

    def to_tuple(self):
        return (self.index, self.name, self.unit, self.range, self.enabled)

    def to_string(self):
        return f"index: {self.index}, name: {self.name}, unit: {self.unit}, range: {self.range}, enabled: {self.enabled}"

    def to_channel_c(self):
        return UnicornAmplifierChannel_C(self.name, self.unit, (c_float * 2)(*self.range), self.enabled)

class UnicornAmplifierConfiguration_C(Structure):
    """
    The type holding an amplifier configuration.
    """
    _fields_ = [
        # The array holding a configuration for each available UNICORN_AMPLIFIER_CHANNEL.
        ("Channels", UnicornAmplifierChannel_C * UNICORN_TOTAL_CHANNELS_COUNT)
        ]

class UnicornAmplifierConfiguration:
    def __init__(self, configuration_c):
        self.channels = list(map(lambda ch_c: UnicornAmplifierChannel(ch_c), configuration_c.Channels))

    def to_dict_shallow(self):
        return {
            "channels": self.channels
        }

    def to_dict_deep(self):
        return {
            "channels": list(map(lambda ch: ch.to_dict(), self.channels))
        }

    def to_dict(self, deep=False):
         return self.to_dict_deep() if deep else self.to_dict_shallow()

    def to_string(self):
        return f"Channels: {str.join('; ', list(map(lambda ch: ch.to_string(), self.channels)))}"

    def to_configuration_c(self):
        c_channels = list(map(lambda ch: ch.to_channel_c(), self.channels))
        return UnicornAmplifierConfiguration_C((UnicornAmplifierChannel_C * len(self.channels))(*c_channels))

class Unicorn:
    def __init__(self):
        self._getApiVersion = lib.UNICORN_GetApiVersion
        self._getApiVersion.restype = c_float

        self._getLastErrorText = lib.UNICORN_GetLastErrorText
        self._getLastErrorText.restype = c_char_p

        self._getBluetoothAdapterInfo = lib.UNICORN_GetBluetoothAdapterInfo
        self._getBluetoothAdapterInfo.restype = c_int
        self._getBluetoothAdapterInfo.argtypes = [POINTER(UnicornBluetoothAdapterInfo)]

        self._getAvailableDevices = lib.UNICORN_GetAvailableDevices
        availableDevices = POINTER(UnicornDeviceSerial)
        availableDevicesCount = POINTER(c_uint32)
        onlyPaired = c_bool
        self._getAvailableDevices.argtypes = [availableDevices, availableDevicesCount, onlyPaired]
        self._getAvailableDevices.restype = c_int

        self._openDevice = lib.UNICORN_OpenDevice
        self._openDevice.argtypes = [c_char_p, POINTER(UnicornHandle)]
        self._openDevice.restype = c_int

        self._closeDevice = lib.UNICORN_CloseDevice
        self._closeDevice.argtypes = [POINTER(UnicornHandle)]
        self._closeDevice.restype = c_int

        self._startAcquisition = lib.UNICORN_StartAcquisition
        self._startAcquisition.argtypes = [UnicornHandle, c_bool]
        self._startAcquisition.restype = c_int

        self._stopAcquisition = lib.UNICORN_StopAcquisition
        self._stopAcquisition.argtypes = [UnicornHandle]
        self._stopAcquisition.restype = c_int

        self._setConfiguration = lib.UNICORN_SetConfiguration
        self._setConfiguration.argtypes = [UnicornHandle, POINTER(UnicornAmplifierConfiguration_C)]
        self._setConfiguration.restype = c_int

        self._getConfiguration = lib.UNICORN_GetConfiguration
        self._getConfiguration.argtypes = [UnicornHandle, POINTER(UnicornAmplifierConfiguration_C)]
        self._getConfiguration.restype = c_int

        self._getData = lib.UNICORN_GetData
        self._getData.argtypes = [UnicornHandle, c_uint32, POINTER(c_float), c_uint32]
        self._getData.restype = c_int

        self._getChannelIndex = lib.UNICORN_GetChannelIndex
        self._getChannelIndex.argtypes = [UnicornHandle, c_char_p, POINTER(c_uint32)]
        self._getChannelIndex.restype = c_int

        self._getNumberOfAcquiredChannels = lib.UNICORN_GetNumberOfAcquiredChannels
        self._getNumberOfAcquiredChannels.argtypes = [UnicornHandle, POINTER(c_uint32)]
        self._getNumberOfAcquiredChannels.restype = c_int

        self._getDeviceInformation = lib.UNICORN_GetDeviceInformation
        self._getDeviceInformation.argtypes = [UnicornHandle, POINTER(UnicornDeviceInformation_C)]
        self._getDeviceInformation.restype = c_int

        self._setDigitalOutputs = lib.UNICORN_SetDigitalOutputs
        self._setDigitalOutputs.argtypes = [UnicornHandle, c_uint8]
        self._setDigitalOutputs.restype = c_int

        self._getDigitalOutputs = lib.UNICORN_GetDigitalOutputs
        self._getDigitalOutputs.argtypes = [UnicornHandle, POINTER(c_uint8)]
        self._getDigitalOutputs.restype = c_int
    
    def getApiVersion(self):
        """Returns the current API version."""
        return self._getApiVersion()

    def getLastErrorText(self):
        """Returns the last error text."""
        return self._getLastErrorText()

    def getBluetoothAdapterInfo(self):
        """
        Retrieves information about the used Bluetooth Adapter.
        An error code is returned as integer if the Bluetooth adapter information could not be acquired.
        """
        adapterHandler = UnicornBluetoothAdapterInfo()
        error = self._getBluetoothAdapterInfo(byref(adapterHandler))

        if error:
            raise UnicornError(error)

        return adapterHandler

    def getAvailableDevices(self, only_paired=True):
        """
        Scans for available devices.
        Discovers available paired or unpaired devices. Estimates the number of available paired 
        or unpaired devices and returns information about discovered devices.
        """
        availableDevices = UnicornDeviceSerial()
        availableDevicesCount = c_uint()
        onlyPaired = c_bool(only_paired)
        # first check if there are any devices available
        error = self._getAvailableDevices(POINTER(UnicornDeviceSerial)(), byref(availableDevicesCount), onlyPaired)

        if error:
            raise UnicornError(error)

        if availableDevicesCount.value < 1:
            # as there no devices, then no more checks are necessary 
            # and we just report current result
            return (availableDevices.value, availableDevicesCount.value)

        # if there are some devices we must retrieve their serials
        availableDevices = (UnicornDeviceSerial * availableDevicesCount.value)()
        availableDevices_ptr = cast(availableDevices, POINTER(UnicornDeviceSerial))
        error = self._getAvailableDevices(availableDevices_ptr, byref(availableDevicesCount), onlyPaired)

        if error:
            raise UnicornError(error)

        device_list = [availableDevices[i].value for i in range(availableDevicesCount.value)]

        return (device_list, availableDevicesCount.value)

    def openDevice(self, serial):
        """
        Opens a device.
        Connects to a certain Unicorn device and assigns a Unicorn handle if the connection attempt succeeded.
        """
        handle = UnicornHandle()
        error = self._openDevice(c_char_p(serial), byref(handle))

        if error:
            raise UnicornError(error)

        return handle.value

    def closeDevice(self, handle):
        """
        Closes a device.
        Disconnects from a device by a given session handle.
        """
        error = self._closeDevice(byref(UnicornHandle(handle)))

        if error:
            raise UnicornError(error)

        return

    def startAcquisition(self, handle, testSignalEnabled=False):
        """
        Initiates a data acquisition in testsignal or measurement mode.
        Starts data acquisition in test signal or measurement mode.
        """
        error = self._startAcquisition(UnicornHandle(handle), c_bool(testSignalEnabled))

        if error:
            raise UnicornError(error)

        return

    def stopAcquisition(self, handle):
        """
        Terminates a running data acquisition.
        Stops a currently running data acquisition session.
        """
        error = self._stopAcquisition(UnicornHandle(handle))

        if error:
            raise UnicornError(error)

        return

    def setConfiguration(self, handle, configuration):
        """
        Sets an amplifier configuration.
        """
        error = self._setConfiguration(UnicornHandle(handle), byref(configuration.to_configuration_c()))

        if error:
            raise UnicornError(error)

        return

    def getConfiguration(self, handle):
        """
        Sets an amplifier configuration.
        """
        configuration = UnicornAmplifierConfiguration_C()
        error = self._getConfiguration(UnicornHandle(handle), byref(configuration))

        if error:
            raise UnicornError(error)

        configuration_py = UnicornAmplifierConfiguration(configuration)
        
        for channel in configuration_py.channels:
            ch_index = self.getChannelIndex(handle=handle, channel_name=channel.name)
            channel.set_index(ch_index)

        return configuration_py

    def getData(self, handle, n_samples, n_channels=None):
        """
        Reads specific number of scans to a specified destination buffer with known length.
        Checks whether the destination buffer is big enough to hold the requested number of scans.
        """
        # if number of channels is not passed, then acquire from all available
        if not n_channels:
            n_channels = len(self.getConfiguration(handle).channels)

        acquisition_buffer_length = n_samples * n_channels
        acquisition_buffer = (c_float * acquisition_buffer_length)()
        error = self._getData(UnicornHandle(handle), c_uint32(n_samples), cast(acquisition_buffer, POINTER(c_float)), c_uint32(acquisition_buffer_length))
        
        if error:
            raise UnicornError(error)

        return acquisition_buffer

    def getChannelIndex(self, handle, channel_name):
        """
        Determines the index of the requested channel in an acquired scan.
        Uses the currently set UNICORN_AMPLIFIER_CONFIGURATION to get the index 
        of the requested channel within an acquired scan.
        """
        channel_index = c_uint32()
        error = self._getChannelIndex(UnicornHandle(handle), c_char_p(channel_name), byref(channel_index))

        if error:
            raise UnicornError(error)

        return channel_index.value

    def getNumberOfAcquiredChannels(self, handle):
        """
        Determines number of acquired channels.
        Uses the currently set UNICORN_AMPLIFIER_CONFIGURATION to get the number of acquired channels.
        """
        n_channels = c_uint32()
        error = self._getNumberOfAcquiredChannels(UnicornHandle(handle), byref(n_channels))

        if error:
            raise UnicornError(error)

        return n_channels.value

    def getDeviceInformation(self, handle):
        """
        Reads the device information by a given UNICORN_HANDLE.
        """
        device_information = UnicornDeviceInformation_C()
        error = self._getDeviceInformation(UnicornHandle(handle), byref(device_information))

        if error:
            raise UnicornError(error)

        return UnicornDeviceInformation(device_information)

    def setDigitalOutputs(self, handle, outputsLevel):
        """
        Sets the digital outputs to high or low.
        """
        error = self._setDigitalOutputs(UnicornHandle(handle), c_uint8(outputsLevel))

        if error:
            raise UnicornError(error)

        return

    def getDigitalOutputs(self, handle):
        """
        Reads the digital output states.
        """
        outputsLevel = c_uint8()
        error = self._getDigitalOutputs(UnicornHandle(handle), byref(outputsLevel))

        if error:
            raise UnicornError(error)

        return outputsLevel


